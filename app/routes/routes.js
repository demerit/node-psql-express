const controller = require("../controllers/controller.js");

var express = require("express");
var router = express.Router();

router.post("/hero", controller.createHero);
router.post("/side", controller.createSidekick);
router.get("/all", controller.findAllHeroes);
router.put("/:name", controller.updateHero);
router.delete("/:name", controller.deleteHero);

module.exports = router;
