const db = require("../models/database");
const Hero = db.heroes;
const Sidekick = db.sidecicks;

exports.createHero = (req, res) => {
  if (!req.body.name) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  const hero = {
    name: req.body.name,
    power: req.body.power,
  };

  Hero.create(hero)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Hero."
      });
    });
};

exports.createSidekick = (req, res) => {
  if (!req.body.name) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  const sidekick = {
    name: req.body.name,
    power: req.body.power,
    hero: req.body.hero,
  };

  Hero.findOne({ where: { name: sidekick.hero } })
      .then(hero => {
        if (hero != null) {
          Sidekick.create({
            name: sidekick.name,
            power: sidekick.power,
            heroId: hero.id,
          }).then((data) => {
            res.send(data);
          }).catch(err => {
            res.status(500).send({
              message:
                  err.message || "Some error occurred while creating the Sidekick."
            });
          });
        }
      }).catch(err => {
        res.status(500).send({
          message:
              err.message || "Some error occurred while creating the Tutorial."
        });
    });
};

exports.findAllHeroes = (req, res) => {
  Hero.findAll({ include: Sidekick })
    .then(data => {
      res.send(JSON.stringify(data, null, 2));
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });
};

exports.updateHero = (req, res) => {
  const name = req.params.name;
  const hero = {
    name: req.body.name,
    power: req.body.power,
  };

  Hero.update(hero, {
    where: { name: name }
  })
    .then(updates => {
      if (updates.length === 1) {
        res.send({
          message: "Hero was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update Hero with name=${name}. Maybe Hero was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Hero with name=" + name
      });
    });
};

exports.deleteHero = (req, res) => {
  const name = req.params.name;

  Hero.destroy({
    where: { name: name }
  })
    .then(num => {
      if (num === 1) {
        res.send({
          message: "Hero was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete Hero with name=${name}. Maybe Hero was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Hero with name=" + name
      });
    });
};
