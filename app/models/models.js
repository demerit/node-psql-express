module.exports = (sequelize, Sequelize) => {
  const Hero = sequelize.define("hero", {
    name: {
      type: Sequelize.STRING
    },
    power: {
      type: Sequelize.STRING
    }
  });

  const Sidekick =  sequelize.define("sidekick", {
    name: {
      type: Sequelize.STRING
    },
    power: {
      type: Sequelize.STRING
    }
  });

  const Villain =  sequelize.define("villain", {
    Name: {
      type: Sequelize.STRING
    },
    power: {
      type: Sequelize.STRING
    }
  });

  Hero.hasMany(Villain);
  Villain.belongsTo(Hero);

  Hero.hasOne(Sidekick, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE'
  });
  Sidekick.belongsTo(Hero);
  return [Hero, Sidekick, Villain];
};
